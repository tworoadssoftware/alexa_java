package org.top_sites;

import sun.misc.BASE64Encoder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.*;

import java.io.FileOutputStream;
import java.io.FileWriter;

/**
 * Makes a request to the Alexa Web Information Service UrlInfo action.
 */
public class alexa_categorylistings {

    private static final String ACTION_NAME = "CategoryListings";
    private static final String RESPONSE_GROUP_NAME = "Listings";
    private static final String SERVICE_HOST = "awis.amazonaws.com";
    private static final String AWS_BASE_URL = "http://" + SERVICE_HOST + "/?";
    private static final String HASH_ALGORITHM = "HmacSHA256";
    
    private static final String Sort_by = "Popularity";     //one of: Popularity | Title | AverageReview
    private static final String Recursive = "True";      //Note that top-level categories will not return any listings unless Recursive=True is specified
    private static final String DATEFORMAT_AWS = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    private String accessKeyId;
    private String secretAccessKey;
    private String path;

    public alexa_categorylistings(String accessKeyId, String secretAccessKey, String path) {
        this.accessKeyId = accessKeyId;
        this.secretAccessKey = secretAccessKey;
        this.path = path;
    }

    /**
     * Generates a timestamp for use with AWS request signing
     *
     * @param date current date
     * @return timestamp
     */
    protected static String getTimestampFromLocalTime(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATEFORMAT_AWS);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return format.format(date);
    }

    /**
     * Computes RFC 2104-compliant HMAC signature.
     *
     * @param data The data to be signed.
     * @return The base64-encoded RFC 2104-compliant HMAC signature.
     * @throws java.security.SignatureException
     *          when signature generation fails
     */
    protected String generateSignature(String data)
            throws java.security.SignatureException {
        String result;
        try {
            // get a hash key from the raw key bytes
            SecretKeySpec signingKey = new SecretKeySpec(
                    secretAccessKey.getBytes(), HASH_ALGORITHM);

            // get a hasher instance and initialize with the signing key
            Mac mac = Mac.getInstance(HASH_ALGORITHM);
            mac.init(signingKey);

            // compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(data.getBytes());

            // base64-encode the hmac
            // result = Encoding.EncodeBase64(rawHmac);
            result = new BASE64Encoder().encode(rawHmac);

        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : "
                    + e.getMessage());
        }
        return result;
    }

    /**
     * Makes a request to the specified Url and return the results as a String
     *
     * @param requestUrl url to make request to
     * @return the XML document as a String
     * @throws IOException
     */
    public static String makeRequest(String requestUrl) throws IOException {
        URL url = new URL(requestUrl);
        URLConnection conn = url.openConnection();
        InputStream in = conn.getInputStream();

        // Read the response
        StringBuffer sb = new StringBuffer();
        int c;
        int lastChar = 0;
        while ((c = in.read()) != -1) {
            if (c == '<' && (lastChar == '>'))
                sb.append('\n');
            sb.append((char) c);
            lastChar = c;
        }
        in.close();
        return sb.toString();
    }


    /**
     * Builds the query string
     */
    protected String buildQuery(int start,int count)
            throws UnsupportedEncodingException {
        String timestamp = getTimestampFromLocalTime(Calendar.getInstance().getTime());

        Map<String, String> queryParams = new TreeMap<String, String>();
        queryParams.put("Action", ACTION_NAME);
        queryParams.put("ResponseGroup", RESPONSE_GROUP_NAME);
        queryParams.put("AWSAccessKeyId", accessKeyId);
        queryParams.put("Timestamp", timestamp);
        queryParams.put("Path", path);
        queryParams.put("Start", "" + start);
        queryParams.put("Count", "" + count);
        queryParams.put("SortBy", Sort_by);
        queryParams.put("SignatureVersion", "2");
        queryParams.put("SignatureMethod", HASH_ALGORITHM);
        queryParams.put("Recursive", Recursive); 

        String query = "";
        boolean first = true;
        for (String name : queryParams.keySet()) {
            if (first)
                first = false;
            else
                query += "&";

            query += name + "=" + URLEncoder.encode(queryParams.get(name), "UTF-8");
        }

        return query;
    }
    
    /**
     * Parses response and prints results
     *
     * @param in stream containing response xml
     */
    private static String parseResponse(InputStream in) throws Exception {
        // Parse the response
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document responseDoc = dbf.newDocumentBuilder().parse(in);

        NodeList responses = responseDoc.getElementsByTagNameNS("*", "Site");
        String siteUrl = "";
        for (int i = 0; i < responses.getLength(); i++) {
            Element response = (Element) responses.item(i);
            Element node = (Element)
                response.getElementsByTagNameNS("*", "DataUrl").item(0);
            siteUrl += node.getFirstChild().getNodeValue()+'\n';
        }
        return(siteUrl);
    }

    /**
     * Makes a request to the Alexa Web Information Service UrlInfo action
     */
    public static void main(String[] args) throws Exception {

        // Read command line parameters

        String accessKey = "AKIAJV6JFRVFEVPMYMRQ";
        String secretKey = "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";
        String path = "Top/Computers/Internet/On_the_Web/Online_Communities/Social_Networking";
        int count = 10;    //Each request maximumly outputs 20 sites
        int max = 50;
        
        FileWriter fw = new FileWriter("output_bycategory.txt", true);
        PrintWriter writer = new PrintWriter(fw);    //"output_bycategory.txt", "UTF-8"
        
        for(int i = 1;i<max;i+=count){
        alexa_categorylistings urlInfo = new alexa_categorylistings(accessKey, secretKey, path);

        String query = urlInfo.buildQuery(i,count);

        String toSign = "GET\n" + SERVICE_HOST + "\n/\n" + query;

        System.out.println("String to sign:\n" + toSign + "\n");

        String signature = urlInfo.generateSignature(toSign);

        String uri = AWS_BASE_URL + query + "&Signature=" +
                URLEncoder.encode(signature, "UTF-8");
        
        // Make the Request

        System.out.println("Making request to:\n");
        System.out.println(uri + "\n");
        
        URL url = new URL(uri);
        URLConnection conn = url.openConnection();
        InputStream in = conn.getInputStream();

        String xmlResponse = makeRequest(uri);
        
        // Print out the Top Sites Listing
        
        System.out.println("Top "+ count +" company sites:\n");
        String siteUrl = parseResponse(in);  
        System.out.println(siteUrl);
        //writer.println(siteUrl);

        // Print out the XML Response

        System.out.println("XML Response:\n");
        System.out.println(xmlResponse);
        
        String writer_text = "Top "+i+" to "+(i+count-1)+" sites:"+"\n"+xmlResponse+"\n";
        writer.println(writer_text);
        }
        
        writer.close();
    }
}