package org.top_sites;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import sun.misc.BASE64Encoder;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.*;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.FileWriter;


/**
 * Simple demo showing how to make a successful request to Alexa Top Sites.
 * <p/>
 * Note that you must sign up for Alexa Top Sites
 * at http://aws.amazon.com/alexatopsites before running this demo.
 */
public class top_sites {
    protected static final String ACTION_NAME = "TopSites";
    protected static final String RESPONSE_GROUP_NAME = "Country";  /*Country*/
    protected static final String SERVICE_HOST = "ats.amazonaws.com";
    protected static final String AWS_BASE_URL = "http://" + SERVICE_HOST + "/?";

    //protected static final int NUMBER_TO_RETURN = 10;
    //protected static final int START_NUMBER = 1001;
    protected static final String DATEFORMAT_AWS = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final String HASH_ALGORITHM = "HmacSHA256";

    private String accessKeyId;
    private String secretAccessKey;
    private String countryCode;

    public top_sites(String accessKeyId, String secretAccessKey, String countryCode) {
        this.accessKeyId = accessKeyId;
        this.secretAccessKey = secretAccessKey;
        this.countryCode = countryCode;
    }

    /**
     * Computes RFC 2104-compliant HMAC signature.
     *
     * @param data data to be signed.
     * @return base64-encoded RFC 2104-compliant HMAC signature.
     * @throws java.security.SignatureException
     *          when signature generation fails
     * It should be noted that the TimeStamp expires after 15 minutes of last time you ran your url
     */
    protected String generateSignature(String data)
        throws java.security.SignatureException {
        String result;
        try {
            // get an hmac key from the raw key bytes
            SecretKeySpec signingKey =
                new SecretKeySpec(secretAccessKey.getBytes(),
                                  HASH_ALGORITHM);

            // get a mac instance and initialize with the signing key
            Mac mac = Mac.getInstance(HASH_ALGORITHM);
            mac.init(signingKey);

            // compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(data.getBytes());

            // base64-encode the hmac
            result = new BASE64Encoder().encode(rawHmac);

        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : "
                                         + e.getMessage());
        }
        return result;
    }

    /**
     * Generates a timestamp for use with AWS request signing
     *
     * @param date current date
     * @return timestamp
     */
    public static String getTimestampFromLocalTime(Date date) {
        SimpleDateFormat format = new SimpleDateFormat(DATEFORMAT_AWS);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        return format.format(date);
    }

    /**
     * Builds the query string
     */
    protected String buildQuery(int start,int count) throws UnsupportedEncodingException {
        String timestamp = getTimestampFromLocalTime(Calendar.getInstance().getTime());

        // TreeMap puts keys in alphabetical order
        Map<String, String> queryParams = new TreeMap<String, String>();
        queryParams.put("Action", ACTION_NAME);
        queryParams.put("ResponseGroup", RESPONSE_GROUP_NAME);
        queryParams.put("AWSAccessKeyId", accessKeyId);
        queryParams.put("Timestamp", URLEncoder.encode(timestamp, "UTF-8"));
        queryParams.put("CountryCode", countryCode);
        queryParams.put("Count", "" + count);
        queryParams.put("Start", "" + start);
        queryParams.put("SignatureVersion", "2");
        queryParams.put("SignatureMethod", HASH_ALGORITHM);

        String query = "";
        boolean first = true;
        for (String name : queryParams.keySet()) {
            if (first)
                first = false;
            else
                query += "&";

            query += name + "=" + queryParams.get(name);
        }

        return query;
    }

    /**
     * Parses response and prints results
     *
     * @param in stream containing response xml
     */
    private static String parseResponse(InputStream in) throws Exception {
        // Parse the response
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document responseDoc = dbf.newDocumentBuilder().parse(in);

        NodeList responses = responseDoc.getElementsByTagNameNS("*", "Site");
        String siteUrl = "";
        for (int i = 0; i < responses.getLength(); i++) {
            Element response = (Element) responses.item(i);
            Element node = (Element)
                response.getElementsByTagNameNS("*", "DataUrl").item(0);
            siteUrl += node.getFirstChild().getNodeValue()+'\n';
        }
        return(siteUrl);
    }

    /**
     * Makes a request to the Alexa Top Sites web service
     */
    public static void main(String[] args) throws Exception {

        String accessKey = "AKIAJV6JFRVFEVPMYMRQ";   
        String secretKey = "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";   
        String countryCode = "US";  /*US*/

        top_sites topSites = new top_sites(accessKey, secretKey, countryCode);
        FileWriter fw = new FileWriter("output_topsites.txt", true);
        PrintWriter writer = new PrintWriter(fw);  //"output_topsites.txt", "UTF-8"
       
        int count = 10;
        int max = 10;
        
        for (int i = 1;i<=max;i+=count){
        String query = topSites.buildQuery(i,count);

        String toSign = "GET\n" + SERVICE_HOST + "\n/\n" + query;

        System.out.println("String to sign:\n" + toSign + "\n");

        String signature = topSites.generateSignature(toSign);

        String uri = AWS_BASE_URL + query +
            "&Signature=" + URLEncoder.encode(signature, "UTF-8");

        // Make request
        System.out.println("\nMaking request to: " + "\n" + uri + "\n");

        URL url = new URL(uri);
        URLConnection conn = url.openConnection();
        InputStream in = conn.getInputStream();
        
        String siteUrl = parseResponse(in);  
        //writer.println(siteUrl);
        System.out.println(siteUrl);
        }
        writer.close();
        
    }

}